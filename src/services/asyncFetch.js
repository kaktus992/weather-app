const asyncFetch = async (url) => {
  const response = await fetch(url);
  if (response.ok) return await response.json();
  throw new Error(response.status);
}

export default asyncFetch;
