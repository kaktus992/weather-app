import asyncFetch from './asyncFetch';
import parseForecastData from './parseForecastData';

const OPEN_WEATHER_API_KEY = '205d41ec0b9fee39918a3d3ad019e6cc';
const OPEN_WEATHER_FORECAST_API_URL = `https://api.openweathermap.org/data/2.5/forecast`
const UNITS_FORMAT = 'metric';

const constructOpenWeatherURL = (cityName) => {
  const queryURL = new URLSearchParams({
    APPID: OPEN_WEATHER_API_KEY,
    units: UNITS_FORMAT,
    q: cityName,
  }).toString();

  return `${OPEN_WEATHER_FORECAST_API_URL}?${queryURL}`;
}

const fetchForecastData = async (cityName) => {
  let response;
  try {
    response = await asyncFetch(constructOpenWeatherURL(cityName));
  } catch({ message }) {
    return { errorMessage: message };
  }

  let { list: forecastData } = response;
  return { forecastData: parseForecastData(forecastData) };
}

export default fetchForecastData;
