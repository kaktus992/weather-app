import moment from 'moment';
import { map, filter } from 'lodash';

const DAILY_FORECAST_ENTRY_HOUR = '12:00';

const getDateFromTimestamp = (timestamp) => moment.unix(timestamp).format('ddd DD/MM/YYYY');

const getWatherIconURL = (iconCode) => `https://openweathermap.org/img/w/${iconCode}.png`;

const parseForecastData = (forecastData) => {
  const filteredForecastData = filter(forecastData, (forecastEntry) => {
    const { dt_txt: forecastTimeUTC } = forecastEntry;
    return forecastTimeUTC.match(DAILY_FORECAST_ENTRY_HOUR);
  });

  return map(filteredForecastData, (forecastEntry) => {
    const {
      clouds: cloudiness,
      dt: forecastTimestamp,
      weather: weatherInfo,
      main: mainForecastMetrics,
      wind,
    } = forecastEntry;

    return {
      cloudiness: cloudiness.all || 0,
      date: getDateFromTimestamp(forecastTimestamp),
      description: weatherInfo[0].description,
      humidity: mainForecastMetrics.humidity,
      iconURL: getWatherIconURL(weatherInfo[0].icon),
      pressure: Math.round(mainForecastMetrics.pressure),
      temperature: Math.round(mainForecastMetrics.temp),
      windSpeed: wind.speed,
    };
  });
};

export default parseForecastData;
