import React from 'react';
import PropTypes from 'prop-types';
import { map } from 'lodash';

import Grid from '@material-ui/core/Grid'

import ForecastCard from '../ForecastCard/ForecastCard';
import './ForecastCardsContainer.css';

const mapForecastDataToNodes = (forecastData) => map(forecastData, (forecastEntry, index) => (
  <Grid item xs={12} md key={index}>
    <ForecastCard
      forecastData={forecastEntry}
    />
  </Grid>
));

const ForecastCardContainer = ({ forecastData }) => {
  const shouldRenderForecastCards = forecastData.length;

  if (!shouldRenderForecastCards) {
    return (
      <p className="ForecastCardsContainer">
        There is no forecast data to display
      </p>
    )
  }

  return (
    <div className="ForecastCardsContainer">
      <Grid container spacing={8}>
        {mapForecastDataToNodes(forecastData)}
      </Grid>
    </div>
  );
}

ForecastCardContainer.propTypes = {
  forecastData: PropTypes.array,
}

ForecastCardContainer.defaultProps = {
  forecastData: [],
}

export default ForecastCardContainer;
