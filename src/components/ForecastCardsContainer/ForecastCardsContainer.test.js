import React from 'react';
import { shallow } from 'enzyme';

import ForecastCardsContainer from './ForecastCardsContainer';
import ForecastCard from '../ForecastCard/ForecastCard';

const { PARSED_FORECAST_DATA } = global.TEST_FIXTURES;

it('renders without crashing', () => {
  const forecastCardsContainerComponent = shallow(<ForecastCardsContainer />);
  expect(forecastCardsContainerComponent).toMatchElement(<ForecastCardsContainer />);
});

it('renders no forecast data info for empty \'forecastData\' property', () => {
  const forecastCardsContainerComponent = shallow(<ForecastCardsContainer />);
  expect(forecastCardsContainerComponent.find('p.ForecastCardsContainer')).toExist();
  expect(forecastCardsContainerComponent.find('p.ForecastCardsContainer'))
    .toHaveText('There is no forecast data to display');
});

it('renders 5 ForecastCards for given forecast data', () => {
  const forecastCardsContainerComponent = shallow(
    <ForecastCardsContainer forecastData={PARSED_FORECAST_DATA}/>
  );
  expect(forecastCardsContainerComponent.find(ForecastCard)).toHaveLength(5);
});

it('renders ForecastCards with forecastData property', () => {
  const forecastCardsContainerComponent = shallow(
    <ForecastCardsContainer forecastData={PARSED_FORECAST_DATA} />
  );

  const forecastCards = forecastCardsContainerComponent.find(ForecastCard);
  forecastCards.forEach((forecastCard, index) => {
    expect(forecastCard).toHaveProp('forecastData', PARSED_FORECAST_DATA[index]);
  })
});

