import React from 'react';
import PropTypes from 'prop-types';

import { Card, CardContent, Grid } from '@material-ui/core';
import './ForecastCard.css';

const ForecastCard = ({ forecastData }) => {
  const {
    cloudiness,
    date,
    description,
    humidity,
    iconURL,
    pressure,
    temperature,
    windSpeed,
  } = forecastData;

  const renderForecastMetric = (metricName, metricValue, metricUnit) => (
    <p className="ForecastCard-single-metric">
      {metricName}: {metricValue} {metricUnit}
    </p>
  );

  return (
    <div className="ForecastCard">
      <Card>
        <CardContent>
          <div className="ForecastCard-title">
            {description}
          </div>
          <div className="ForecastCard-date">
            {date}
          </div>
          <Grid container>
            <Grid item xs={12}>
              <div className="ForecastCard-temp">
                <img src={iconURL} alt="forecast icon" className="ForecastCard-temp-icon"/>
                <span className="ForecastCard-temp-text">{temperature}&deg;C</span>
              </div>
            </Grid>
            <Grid item xs={6} sm md={12}>
              {renderForecastMetric('Cloudiness', cloudiness, '%')}
            </Grid>
            <Grid item xs={6} sm md={12}>
              {renderForecastMetric('Humidity', humidity, '%')}
            </Grid>
            <Grid item xs={6} sm md={12}>
              {renderForecastMetric('Wind', windSpeed, 'm/s')}
            </Grid>
            <Grid item xs={6} sm md={12}>
              {renderForecastMetric('Pressure', pressure, 'hPa')}
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </div>
  );
}

ForecastCard.propTypes = {
  forecastData: PropTypes.object,
}

ForecastCard.defaultProps = {
  forecastData: {},
}

export default ForecastCard;
