import React from 'react';
import { shallow } from 'enzyme';

import ForecastCard from './ForecastCard';
import { Card, CardContent } from '@material-ui/core';

it('renders without crashing', () => {
  const forecastCardComponent = shallow(<ForecastCard />);
  expect(forecastCardComponent).toMatchElement(<ForecastCard />);
});

it('renders <div> with \'ForecastCard\' className', () => {
  const forecastCardComponent = shallow(<ForecastCard />);
  expect(forecastCardComponent.find('div.ForecastCard')).toExist();
});

it('renders component using Card and CardContent material-ui components', () => {
  const forecastCardComponent = shallow(<ForecastCard />);
  expect(forecastCardComponent.find(Card)).toExist();
  expect(forecastCardComponent.find(CardContent)).toExist();
});

it('renders forecast title, date and temperature', () => {
  const forecastCardComponent = shallow(<ForecastCard />);
  expect(forecastCardComponent.find('div.ForecastCard-title')).toExist();
  expect(forecastCardComponent.find('div.ForecastCard-date')).toExist();
  expect(forecastCardComponent.find('div.ForecastCard-temp')).toExist();
});

it('renders forecast icon', () => {
  const forecastCardComponent = shallow(<ForecastCard />);
  expect(forecastCardComponent.find('img.ForecastCard-temp-icon')).toExist();
});

it('renders forecast temprature with degree sign', () => {
  const forecastCardComponent = shallow(<ForecastCard />);
  expect(forecastCardComponent.find('span.ForecastCard-temp-text')).toIncludeText('°C');
});

it('renders 4 metrics values containers', () => {
  const forecastCardComponent = shallow(<ForecastCard />);
  expect(forecastCardComponent.find('p.ForecastCard-single-metric')).toHaveLength(4);
});
