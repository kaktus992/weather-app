import React from 'react';
import { shallow } from 'enzyme';

import App from './App';
import ErrorMessage from '../ErrorMessage/ErrorMessage';
import LocationSearch from '../LocationSearch/LocationSearch';

import fetchForecastData from '../../services/fetchForecastData';
jest.mock('../../services/fetchForecastData');

const { PARSED_FORECAST_DATA } = global.TEST_FIXTURES;

describe('fetch forecast data correctly', () => {
  beforeAll(() => {
    fetchForecastData.mockImplementation(() => ({
      errorMessage: '',
      forecastData: PARSED_FORECAST_DATA,
    }));
  });

  afterEach(() => {
    fetchForecastData.mockRestore();
  });

  it('renders without crashing', () => {
    const appComponent = shallow(<App />);
    expect(appComponent).toMatchElement(<App />);
  });

  it('sets default state', () => {
    const appComponent = shallow(<App />);

    expect(appComponent.state('errorMessage')).toEqual('');
    expect(appComponent.state('cityName')).toEqual('London');
    expect(appComponent.state('additonalLocationInfo')).toEqual('UK');
    expect(appComponent.state('forecastData')).toEqual([]);
    expect(appComponent.state('isFetching')).toEqual(true);
  });

  it('calls componentDidMount', () => {
    const componentDidMountSpy = jest.spyOn(App.prototype, 'componentDidMount');
    shallow(<App />);

    expect(componentDidMountSpy).toBeCalled();
  });

  it('calls updateForecastData in componentDidMount', () => {
    const appComponent = shallow(<App />);
    const updateForecastDataSpy = jest.spyOn(appComponent.instance(), 'updateForecastData');

    appComponent.instance().componentDidMount();

    expect(updateForecastDataSpy).toBeCalled();
  });

  it('mutates state after componentDidMount', () => {
    const appComponent = shallow(<App />);

    return Promise.resolve().then(() => {
      expect(appComponent.state('errorMessage')).toEqual('');
      expect(appComponent.state('forecastData')).toEqual(PARSED_FORECAST_DATA);
      expect(appComponent.state('isFetching')).toEqual(false);
    });
  });

  it('mutates setState on handleLocationChange with locationData', () => {
    const appComponent = shallow(<App />);

    appComponent.instance().handleLocationChange('New York, NY, USA')

    expect(appComponent.state('additonalLocationInfo')).toEqual('NY, USA');
    expect(appComponent.state('cityName')).toEqual('New York');
    expect(appComponent.state('isFetching')).toEqual(true);
  });

  it('calls updateForecastData after handleLocationChange', () => {
    const appComponent = shallow(<App />);
    const updateForecastDataSpy = jest.spyOn(appComponent.instance(), 'updateForecastData');

    appComponent.instance().handleLocationChange('New York, NY, USA')

    expect(updateForecastDataSpy).toBeCalled();
  });

  it('mutates state on LocationSearch change', () => {
    const appComponent = shallow(<App />);
    const locationSearchWrapper = appComponent.find(LocationSearch);

    locationSearchWrapper.simulate('change', 'New York, NY, USA');

    expect(appComponent.state('additonalLocationInfo')).toEqual('NY, USA');
    expect(appComponent.state('cityName')).toEqual('New York');
    expect(appComponent.state('isFetching')).toEqual(true);
  });
});

describe('throws an Error during data fetch', () => {
  beforeAll(() => {
    fetchForecastData.mockImplementation(() => ({
      errorMessage: '404',
      forecastData: [],
    }));
  });

  afterEach(() => {
    fetchForecastData.mockRestore();
  });

  it('sets proper errorMessage in component state', () => {
    const appComponent = shallow(<App />);

    return Promise.resolve().then(() => {
      expect(appComponent.state('errorMessage')).toEqual('404');
      expect(appComponent.state('forecastData')).toEqual([]);
      expect(appComponent.state('isFetching')).toEqual(false);
    });
  });

  it('render ErrorMessage with proper message property', () => {
    const appComponent = shallow(<App />);

    return Promise.resolve().then(() => {
      appComponent.update();
      const errorMessageWrapper = appComponent.find(ErrorMessage);

      expect(errorMessageWrapper).toExist();
      expect(errorMessageWrapper).toHaveProp('message');
    });
  });
});

