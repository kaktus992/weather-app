import React, { Component, Fragment } from 'react';

import { CircularProgress, Divider } from '@material-ui/core';
import { ReactIcon, MaterialUiIcon, WeatherFogIcon } from 'mdi-react';

import ErrorMessage from '../ErrorMessage/ErrorMessage';
import ForecastCardsContainer from '../ForecastCardsContainer/ForecastCardsContainer';
import LocationSearch from '../LocationSearch/LocationSearch';

import fetchForecastData from '../../services/fetchForecastData';
import './App.css';

const extractLocationData = (location) => {
  const splittedLocationData = location.split(/, (.+)/);

  return {
    cityName: splittedLocationData[0],
    additonalLocationInfo: splittedLocationData[1],
  }
};

class App extends Component {
  constructor() {
    super();
    this.state = {
      errorMessage: '',
      cityName: 'London',
      additonalLocationInfo: 'UK',
      forecastData: [],
      isFetching: true,
    }
  }

  async componentDidMount() {
    this.updateForecastData();
  }

  async updateForecastData() {
    const { errorMessage, forecastData } = await fetchForecastData(this.state.cityName);

    this.setState({
      errorMessage,
      forecastData,
      isFetching: false,
    });
  }

  handleLocationChange = (location) => {
    const locationStateData = extractLocationData(location);
    this.setState(Object.assign(
      locationStateData,
      { isFetching: true },
    ), this.updateForecastData);
  }

  renderAppHeader() {
    return (
      <div className="App-header">
        <div className="App-name">
          <WeatherFogIcon />
          <span>
            Weather App
          </span>
        </div>
        <div className="App-headline">
          Built with
          <a
            className="App-headline-react"
            href="https://reactjs.org/"
            target="_blank"
            rel="noopener noreferrer">
            <ReactIcon />React.js
          </a>
          and
          <a
            className="App-headline-material-ui"
            href="https://material-ui.com/"
            target="_blank"
            rel="noopener noreferrer">
            <MaterialUiIcon />Material-UI
          </a>
        </div>
      </div>
    )
  }

  renderLocationInfo() {
    const { additonalLocationInfo, cityName } = this.state;
    return (
      <Fragment>
        <p className="App-city-name">{cityName}</p>
        <p className="App-location-info">{additonalLocationInfo}</p>
      </Fragment>
    )
  }

  renderForecastCards() {
    const { errorMessage, forecastData, isFetching } = this.state;
    if (isFetching) {
      return (
        <div className="App-progress">
          <CircularProgress size={150} />;
        </div>
      );
    }

    if (errorMessage) {
      return <ErrorMessage message={errorMessage} />
    }

    return <ForecastCardsContainer forecastData={forecastData} />;
  }

  render() {
    return (
      <div className="App">
        {this.renderAppHeader()}
        <Divider />
        <LocationSearch onChange={this.handleLocationChange} />
        {this.renderLocationInfo()}
        {this.renderForecastCards()}
      </div>
    );
  }
}

export default App;
