import React from 'react';

import './ErrorMessage.css';

const ERROR_404_COPY = 'Could not find forecast for given city.';
const ERROR_FETCH_COPY = 'Could not fetch forecast data. Please try again later.';

const getErrorMessageCopy = (message) => (
  message === '404'? ERROR_404_COPY : ERROR_FETCH_COPY
);

const ErrorMessage = ({ message }) => {
  return (
    <p className="ErrorMessage">
      {getErrorMessageCopy(message)}
    </p>
  );
}

export default ErrorMessage;
