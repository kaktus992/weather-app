import React from 'react';
import { shallow } from 'enzyme';

import ErrorMessage from './ErrorMessage';

it('renders without crashing', () => {
  const errorMessageComponent = shallow(<ErrorMessage />);
  expect(errorMessageComponent).toMatchElement(<ErrorMessage />);
});

it('renders ErrorMessage with proper copy for 404 error message', () => {
  const errorMessageComponent = shallow(<ErrorMessage message='404'/>);
  expect(errorMessageComponent.find('p')).toHaveText('Could not find forecast for given city.');
});

it('renders ErrorMessage with proper copy for \'Failed to fetch\' copy', () => {
  const errorMessageComponent = shallow(<ErrorMessage message='Failed to fetch.' />);
  expect(errorMessageComponent.find('p')).toHaveText('Could not fetch forecast data. Please try again later.');
});
