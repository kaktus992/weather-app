import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PlacesAutocomplete from 'react-places-autocomplete';
import classNames from 'classnames';
import { noop } from 'lodash';

import './LocationSearch.css';

class LocationSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      address: '',
    };
  }

  handleChange = (address) => {
    this.setState({
      address,
    });
  };

  handleSelect = (selected) => {
    this.setState({ address: selected }, () => {
      this.props.onChange(selected);
    });
  };

  handleCloseClick = () => {
    this.setState({
      address: '',
    });
  };

  handleError = (status, clearSuggestions) => {
    console.log('Error from Google Maps API', status); // eslint-disable-line no-console
    clearSuggestions();
  };

  renderClearButton() {
    if (!this.state.address.length) {
      return null;
    }

    return (
      <button
        className="LocationSearch-clear-button"
        onClick={this.handleCloseClick}>
        x
      </button>
    );
  }

  renderSuggestionItems(suggestions, getSuggestionItemProps) {
    if (!suggestions.length) {
      return null;
    }

    return (
      <div className="LocationSearch-autocomplete-container">
        {suggestions.map(suggestion => {
          const className = classNames('LocationSearch-suggestion-item', {
            'LocationSearch-suggestion-item--active': suggestion.active,
          });

          const suggestionItemProps = getSuggestionItemProps(suggestion, { className })

          return (
            <div {...suggestionItemProps}>
              <strong>
                {suggestion.formattedSuggestion.mainText}
              </strong>
              {' '}
              <small>
                {suggestion.formattedSuggestion.secondaryText}
              </small>
            </div>
          );
        })}
      </div>
    )
  }

  render() {
    const {
      address,
    } = this.state;

    const searchOptions = {
      types: ['(cities)'],
    }

    return (
      <div>
        <PlacesAutocomplete
          onChange={this.handleChange}
          value={address}
          onSelect={this.handleSelect}
          onError={this.handleError}
          searchOptions={searchOptions}
        >
          {({ getInputProps, suggestions, getSuggestionItemProps }) => {
            const inputProps = getInputProps({
              placeholder: 'Search Places...',
              className: 'LocationSearch-search-input',
            });

            return (
              <div className="LocationSearch-search-bar-container">
                <div className="LocationSearch-search-input-container">
                  <input {...inputProps}/>
                  {this.renderClearButton()}
                </div>
                {this.renderSuggestionItems(suggestions, getSuggestionItemProps)}
              </div>
            );
          }}
        </PlacesAutocomplete>
      </div>
    );
  }
}

LocationSearch.propTypes = {
  onChange: PropTypes.func,
};

LocationSearch.defaultProps = {
  onChange: noop,
};

export default LocationSearch;
