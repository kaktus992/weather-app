import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import 'jest-enzyme';

configure({ adapter: new Adapter() });

global.TEST_FIXTURES = {
  PARSED_FORECAST_ENTRY: {
    cloudiness: 0,
    date: 'Thu 21/06/2018',
    description: 'clear sky',
    humidity: 59,
    iconURL: 'https://openweathermap.org/img/w/01d.png',
    pressure: 1032,
    temperature: 17,
    windSpeed: 4.85,
  },
  PARSED_FORECAST_DATA: [
    {
      cloudiness: 0,
      date: 'Thu 21/06/2018',
      description: 'clear sky',
      humidity: 59,
      iconURL: 'https://openweathermap.org/img/w/01d.png',
      pressure: 1032,
      temperature: 17,
      windSpeed: 4.85,
    },
    {
      cloudiness: 0,
      date: 'Fri 22/06/2018',
      description: 'clear sky',
      humidity: 56,
      iconURL: 'https://openweathermap.org/img/w/01d.png',
      pressure: 1036,
      temperature: 19,
      windSpeed: 3.17,
    },
    {
      cloudiness: 12,
      date: 'Sat 23/06/2018',
      description: 'few clouds',
      humidity: 54,
      iconURL: 'https://openweathermap.org/img/w/02d.png',
      pressure: 1033,
      temperature: 20,
      windSpeed: 3.12,
    },
    {
      cloudiness: 0,
      date: 'Sun 24/06/2018',
      description: 'clear sky',
      humidity: 51,
      iconURL: 'https://openweathermap.org/img/w/01d.png',
      pressure: 1033,
      temperature: 21,
      windSpeed: 1.96,
    },
    {
      cloudiness: 0,
      date: 'Mon 25/06/2018',
      description: 'clear sky',
      humidity: 47,
      iconURL: 'https://openweathermap.org/img/w/01d.png',
      pressure: 1032,
      temperature: 24,
      windSpeed: 1.86,
    },
  ],
}