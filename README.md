# Weather App
An exercive Weather App built with [React.js](https://reactjs.org/), and [Material-UI](https://material-ui.com/).


## Demo

https://mgolawski.github.io/weather-app/

## Quick Overview

The project sketch was created with [create-react-app](https://github.com/facebook/create-react-app).

Forecast data is provided by [OpenWeatherMap forecast API](https://openweathermap.org/forecast5)

Places data is provided by [Google Places](https://cloud.google.com/maps-platform/places/).

## Usage

### `yarn install`

Installs all the required dependencies, including testing tools, and react-scripts.

### `npm start` or `yarn start`

Runs the app in development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will automatically reload if you make changes to the code.
You will see the build errors, and lint warnings in the console.

### `npm test` or `yarn test`

Runs the test watcher in an interactive mode.

By default, runs tests related to files changed since the last commit.

[Read more about testing.](https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/template/README.md#running-tests)

### `npm run build` or `yarn build`

Builds the app for production to the `build` folder.

It correctly bundles React in production mode, and optimizes the build for the best performance.

The build is minified, and the filenames include the hashes.

By default, it also [includes a service worker](https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/template/README.md#making-a-progressive-web-app) so that your app loads from local cache on future visits.

### `npm deploy` or `yarn deploy`

I use [GitHub Pages](https://pages.github.com/) for easy hosting directly from Github repository.

Package.json contains `homepage` field to determine the root URL in the built HTML file.
```
"homepage": "http://mgolawski.github.io/weather-app",
```

## Tradeoffs

I used `create-react-app` for out of the box configuration, and single build dependency. It is a great react boilerplate for an exercise or small/medium projects. For bigger websites I would build custom stack, based on project requirements.

I choosed [Places Autocomplete](https://github.com/kenny-hibino/react-places-autocomplete) for easy location search implementation. It works nice, but it forced me to use not very elegant, and hard to test code. If I had more time for development I would definitely implement such component by myself. That's why I resigned to cover that code with tests.

I resigned also to cover services with tests. Their logic is simple, and I decided that I would focus on components test. I used [Jest](https://facebook.github.io/jest/) for test running, and [Enzyme](https://github.com/airbnb/enzyme) for isolation components tests. I am aware that there are couple more test cases to cover, but I think that given test set will give you enough insight on my testing skills.

There are some API keys(Google API, OpenWeatherMap API), that are hardcoded inside the HTML or JS files. I resigned from the safety steps, and decided to keep that. Of course for production projects such keys should be served in a safer way. For this exercise I decided to not focus on such problems.

Most of the components are stateless, and functional. I tried to keep it that way to avoid complicated logic, and keep it performance efficient. In bigger project, such choice would let me use [babel plugins](https://babeljs.io/docs/en/babel-plugin-transform-react-inline-elements/) to improve rendering performance. Also there is announced bigger support for functional components in the future.

## User experience
The view is very simple, and provide weather forecast for the next 5 days, during noon hours. I choosed that scenario for clean, and transparent forecast. The tradeoff is lack of current weather data. That's why the next thing to implement should be the current weather information above ForecastCards section.

I would love to see there the detailed forecast data for the whole day. OpenWeatherMap API endpoint gives us opportunity to use weather data per every 3 hours. That would allows to implement elegant temperature graph with [vx](https://vx-demo.now.sh/). It could be located in the expandable part of ForecastCard component.

I picked Material-UI for it's consistency, and because it is well understandable across web community. It allowed me to focus directly on the code, and provide clean, and simple user interface.

## Global packages

The project should not use any global package instead of proper package manager. I suggest to use `yarn`, but `npm` is also supported.

During development I spotted the problem with testing scripts. The test watch scripts halted and throwed errors. Installing the watchman fixed the problem.

```
brew install watchman
```

## The end

If you need the Github repository access for any purpose, please contact with me via email: `golawskimarcin@gmail.com`. If you also have any questions, feel free to contant.

## [Github](https://github.com/mgolawski)